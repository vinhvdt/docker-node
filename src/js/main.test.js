const {sum, minus, multiply, divide} = require('./main');

test('minus 3 - 2 to equal 1', () => {
    expect(minus(3, 2)).toBe(1);
});

test('adds 1 + 2 to equal 3', () => {
  expect(sum(1, 2)).toBe(3);
});

test('multiply 2 * 3 to equal 6', () => {
    expect(multiply(2, 3)).toBe(6);
});

test('divide 6 / 2 to equal 3', () => {
    expect(divide(6,2)).toBe(3)
});