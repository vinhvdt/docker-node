const express = require('express');

const app = express();
const port = process.env.PORT;

app.use(express.static(`${__dirname}`));

app.get('/', (req, res) => {  
  res
    .status(200)
    .json({message: 'Hello from the server side', app: 'MealFinder'});
});

app.listen(port, () => {  
  console.log(`App running on port in container is ${port}`);
});