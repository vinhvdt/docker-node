# Deloy as base
FROM node:17-alpine as base
WORKDIR /app


# Deloy for dev CI/CD
FROM base as dev
COPY src/package.json src/package-lock.json /app/
RUN npm install
COPY ./src /app/
CMD ["node", "index.js"] 


# Deloy for production server nodejs
FROM base as prod
COPY src/package.json src/package-lock.json /app/
RUN npm install --only=production
COPY ./src /app/
ENV PORT=3000
CMD ["node", "index.js"]

# Deloy for production server nginx (thuong dung cho cac projetc thuan front end (react, vue, ...))
FROM nginx:1.21.6-alpine
COPY --from=dev /app/ /usr/share/nginx/html

# Can trien khai test trong docker-compose
# Can trien khai o production dung server nginx



